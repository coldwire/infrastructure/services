datacenter = "dc1"

data_dir   = "/opt/nomad"
plugin_dir = "/opt/plugins"

addresses {
  http = "0.0.0.0"
  rpc  = "0.0.0.0"
  serf = "0.0.0.0"
} 

advertise {
  http = "0.0.0.0"
  rpc  = "0.0.0.0"
  serf = "0.0.0.0"
}

client {
  enabled = true
}

server {
  enabled = true
  bootstrap_expect = 1
}

consul {
  address = "127.0.0.1:8500"
}

plugin "nomad-driver-podman" {
  config {
    socket_path = "unix:///var/run/podman/podman.sock"
  }
}