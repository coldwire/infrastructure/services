datacenter = "dc1"

data_dir = "/opt/consul"

ui_config{
  enabled = true
}

bind_addr   = "0.0.0.0"
client_addr = "0.0.0.0"

server = true

bootstrap_expect = 1