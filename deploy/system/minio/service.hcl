job "cw-minio" {
  datacenters = ["dc1"]
  priority = 60

  group "cw-minio-server" {
    count = 1

    network {
      port "http" {
        static = 9090
      }

      port "s3" {
        to = 9000
      }
    }

    service {
      name = "cw-minio-server"
      port = "http"

      check {
        type     = "http"
        path     = "/"
        interval = "2s"
        timeout  = "2s"
      }
    }

    service {
      name = "cw-minio-s3"
      port = "s3"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.cw-minio.rule=Host(`s3.coldnet.org`)",
        "traefik.http.routers.cw-minio.tls=true",
        "traefik.http.routers.cw-minio.tls.certresolver=letsencrypt",
      ]

      check {
        type     = "http"
        path     = "/minio/health/live"
        interval = "2s"
        timeout  = "2s"
      }      
    }

    task "cw-minio-server" {
      driver = "podman"

      config {
        image = "quay.io/minio/minio"
        ports = ["http", "s3"]

        command = "server"
        args = [
          "/data-{1...2}",
          "--console-address",
          "0.0.0.0:9090"
        ]

        volumes = [
          "/mnt/data1/minio:/data-1",
          "/mnt/data2/minio:/data-2"
        ]
      }

      env {
        MINIO_ROOT_USER = "root"
        MINIO_VOLUMES="/data-{1...2}"
        MINIO_SERVER_URL="https://s3.coldnet.org"
      }

      resources {
        cpu = 1000
        memory = 4096
      }

      template {
        data = file("./env.tpl")
        destination = "secrets/env" 
        env = true
      }

      vault {
        policies = ["cw-minio"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }
    }
  }
}