job "cw-stolon" {
  datacenters = ["dc1"]

  group "cw-stolon-sentinel" {
    count = 1

    task "sentinel" {
      driver = "podman"

      config {
        image = "docker://coldwireorg/postgres:v0.0.1"
        network_mode = "host"
        command = "/usr/local/bin/stolon-sentinel"
        args = [
          "--cluster-name=cw-stolon",
          "--store-backend=consul",
          "--store-endpoints=http://${attr.unique.network.ip-address}:8500"
        ]
      }
    }
  }

  group "cw-stolon-keepers" {
    count = 1

    network {
      port "keeper-1" {
        static = 6431
      }

      port "keeper-2" {
        static = 6432
      }
    }

    task "keeper-1" {
      driver = "podman"

      service {
        name = "stolon-keeper"
        port = "keeper-1"
        check {
          type = "tcp"
          port = "keeper-1"
          interval = "60s"
          timeout = "5s"
        }
      }

      config {
        image = "docker://coldwireorg/postgres:v0.0.1"
        network_mode = "host"
        command = "/usr/local/bin/stolon-keeper"
        readonly_rootfs = false
        args = [
          "--cluster-name=cw-stolon",
          "--store-backend=consul",
          "--store-endpoints=http://${attr.unique.network.ip-address}:8500",
          "--data-dir=/mnt/persist",
          "--pg-listen-address=${NOMAD_IP_keeper_1}",
          "--pg-port=${NOMAD_PORT_keeper_1}",
          "--pg-su-password=${PG_SU_PWD}",
          "--pg-repl-username=repluser",
          "--pg-repl-password=${PG_REPL_PWD}",
          "--disable-data-dir-locking",
          "--uid=001"
        ]

        ports = [ "keeper-1" ]
        volumes = [
          "/mnt/data1/stolon:/mnt/persist:rw",
          "/etc/localtime:/etc/localtime:ro"
        ]
      }

      user = 1002

      resources {
        cpu = 1000
        memory = 4000
      }

      template {
        data = file("./env.tpl")
        destination = "secrets/env"
        env = true
      }

      vault {
        policies = ["cw-stolon"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }
    }

    task "keeper-2" {
      driver = "podman"

      service {
        name = "stolon-keeper"
        port = "keeper-2"
        check {
          type = "tcp"
          port = "keeper-2"
          interval = "60s"
          timeout = "5s"
        }
      }

      config {
        image = "docker://coldwireorg/postgres:v0.0.1"
        network_mode = "host"
        command = "/usr/local/bin/stolon-keeper"
        args = [
          "--cluster-name=cw-stolon",
          "--store-backend=consul",
          "--store-endpoints=http://${attr.unique.network.ip-address}:8500",
          "--data-dir=/mnt/persist",
          "--pg-listen-address=${NOMAD_IP_keeper_2}",
          "--pg-port=${NOMAD_PORT_keeper_2}",
          "--pg-su-password=${PG_SU_PWD}",
          "--pg-repl-username=repluser",
          "--pg-repl-password=${PG_REPL_PWD}",
          "--disable-data-dir-locking",
          "--uid=002"
        ]

        ports = [ "keeper-2" ]
        volumes = [
          "/mnt/data2/stolon:/mnt/persist:rw",
          "/etc/localtime:/etc/localtime:ro"
        ]
      }

      user = 1002

      resources {
        cpu = 1000
        memory = 4000
      }

      template {
        data = file("./env.tpl")
        destination = "secrets/env"
        env = true
      }

      vault {
        policies = ["cw-stolon"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }
    }
  }

  group "proxy" {
    network {
      port "proxy" {
        static = 5432
      }
    }

    task "proxy" {
      driver = "podman"

      config {
        image = "docker://coldwireorg/postgres:v0.0.1"
        network_mode = "host" 
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-proxy" 
        args = [
          "--cluster-name=cw-stolon",
          "--store-backend=consul",
          "--store-endpoints=http://${attr.unique.network.ip-address}:8500",
          "--port=${NOMAD_PORT_proxy}",
          "--listen-address=${NOMAD_IP_proxy}",
          "--log-level=info"
        ]

        ports = ["proxy"]
      }

      resources {
        memory = 100
      }

      service {
        name = "cw-stolon-proxy"
        port = "proxy"
        address_mode = "host"
        check {
          type = "tcp"
          port = "proxy"
          interval = "60s"
          timeout = "5s"
        }
      }
    }
  }
}