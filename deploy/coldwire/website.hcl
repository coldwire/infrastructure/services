job "cw-website" {
  datacenters = ["dc1"]
  priority = 60

  group "cw-website" {
    count = 1

    network {
      port "http" {
        to = 5173
      }
    }

    service {
      name = "cw-website-server"
      port = "http"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.cw-website.rule=Host(`coldwire.org`)",
        "traefik.http.routers.cw-website.tls=true",
        "traefik.http.routers.cw-website.tls.certresolver=letsencrypt",
      ]

      check {
        type     = "http"
        path     = "/"
        interval = "2s"
        timeout  = "2s"
      }
    }

    task "cw-website-server" {
      driver = "podman"

      config {
        image = "docker://coldwireorg/website:v1.1.7"
        ports = ["http"]
      }
    }
  }
}
