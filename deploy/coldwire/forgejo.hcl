job "cw-forgejo" {
  datacenters = ["dc1"]
  priority = 60

  group "cw-forgejo" {
    count = 1

    network {
      port "http" {
        to = 8043
      }

      port "ssh" {
        to = 8022
      }
    }

    service {
      name = "cw-forgejo-server"
      port = "http"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.cw-forgejo.rule=Host(`git.coldwire.org`)",
        "traefik.http.routers.cw-forgejo.tls=true",
        "traefik.http.routers.cw-forgejo.tls.certresolver=letsencrypt",
      ]

      check {
        type     = "http"
        path     = "/"
        interval = "2s"
        timeout  = "2s"
      }
    }

    task "cw-forgejo-server" {
      driver = "podman"

      config {
        image = "docker://codeberg.org/forgejo/forgejo:1.18.0-1"
        ports = ["http", "ssh"]
      }

      env {
        GITEA__storage__STORAGE_TYPE=minio
        GITEA__storage__MINIO_ENDPOINT=localhost:3900
        GITEA__storage__MINIO_ACCESS_KEY_ID=Ju14T1EQzBLVIsDE
        GITEA__storage__MINIO_SECRET_ACCESS_KEY=wFW6L1YI62V4XSdiv62vHUAdJ9ISA3K2
        GITEA__storage__MINIO_BUCKET=cw-gitea
        GITEA__storage__MINIO_USE_SSL=
        DB_TYPE = "postgres"
        DB_HOST = "${NOMAD_ADDR_db_db}"
        DB_NAME = "gitea"
        DB_USER = "gitea"
        DB_PASSWD  = "gitea"
      }
    }
  }
}




job "cw- gitea" {
  datacenters = ["dc1"]
  priority = 60
  
  group "svc" {
    count = 1
    
    volume "gitea-data" {
      type      = "host"
      source    = "gitea-data"
      read_only = false
    }
    volume "gitea-db" {
      type      = "host"
      source    = "gitea-db"
      read_only = false
    }
    restart {
      attempts = 5
      delay    = "30s"
    }
    
    task "app" {
      driver = "docker"
      
      volume_mount {
        volume      = "gitea-data"
        destination = "/data"
        read_only   = false
      }
      
      config {
        image = "gitea/gitea:linux-arm64"
        
        port_map {
          http     = 3000
          ssh_pass = 22
        }
      }
      
      env = {
        "APP_NAME"   = "Gitea: Git with a cup of tea"
        "RUN_MODE"   = "prod"
        "SSH_DOMAIN" = "git.example.com"
        "SSH_PORT"   = "22"
        "ROOT_URL"   = "http://git.example.com/"
        "USER_UID"   = "1002"
        "USER_GID"   = "1002"
        "DB_TYPE"    = "postgres"
        "DB_HOST"    = "${NOMAD_ADDR_db_db}"
        "DB_NAME"    = "gitea"
        "DB_USER"    = "gitea"
        "DB_PASSWD"  = "gitea"
      }
      
      resources {
        cpu    = 200
        memory = 256
        network {
          port "http" {}
          port "ssh_pass" {
            static = "2222"
          }
        }
      }

      service {
        name = "gitea-gui"
        port = "http"
      }
    }
    
    task "db" {
      driver = "docker"
      
      volume_mount {
        volume      = "gitea-db"
        destination = "/var/lib/postgresql/data"
        read_only   = false
      }
      
      config {
        image = "postgres:10-alpine"
        
        port_map {
          db = 5432
        }
      }
      
      env {
        "POSTGRES_USER"     = "gitea"
        "POSTGRES_PASSWORD" = "gitea"
        "POSTGRES_DB"       = "gitea"
      }
      
      resources {
        cpu    = 200
        memory = 128
        
        network {
          port "db" {}
        }
      }
    }
  }
}